export class UserModel {
  public username: String;
  public email: String;

  constructor(username: String, email: String) {
    this.username = username;
    this.email = email;
  }
}