import { Component, OnInit }        from '@angular/core';
import { AuthService }              from '../../services/auth.service';
import { UserModel }                from '../../shared/models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  name;
  vorname;
  email;
  public user: UserModel;
  imageUrl = '../../../assets/images/background2.jpg';

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.name = profile.user.name;
      this.vorname = profile.user.vorname;
      this.email = profile.user.email;
    })
  }



}