import { Component, OnInit }                  from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService }                        from '../../services/auth.service';
import { Router }                             from '@angular/router';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html"
})
export class RegisterComponent implements OnInit {

  message: String;
  messageClass: String;
  processing = false;
  emailValid;
  emailMessage;
  usernameValid;
  usernameMessage;
  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.userForm = this.formBuilder.group(
      {
        email: [
          "",
          Validators.compose([
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(30),
            Validators.email
          ])
        ],
        username: [
          "",
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(15),
            this.validateUsername
          ])
        ],
        password: [
          "",
          Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(35),
            this.validatePassword
          ])
        ],
        confirm: ["", Validators.required],
        name: ['', Validators.required],
        vorname: ['', Validators.required]
      },
      { validator: this.matchingPasswords("password", "confirm") }
    );
  }

  validateUsername(controls) {
    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { validateUsername: true };
    }
  }

  validatePassword(controls) {
    const regExp = new RegExp(
      /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/
    );
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { validatePassword: true };
    }
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {
      if (group.controls[password].value === group.controls[confirm].value) {
        return null;
      } else {
        return { matchingPasswords: true };
      }
    };
  }

  registerSubmit() {
    let user = {
      email: this.userForm.get('email').value,
      username: this.userForm.get('username').value,
      password: this.userForm.get('password').value,
      name: this.userForm.get('name').value,
      vorname: this.userForm.get('vorname').value
    }
    this.authService.registerUser(user).subscribe(data => {
      if (!data.success) {
        this.message = data.message;
        this.messageClass = 'alert alert-error';
        this.processing = false;
      } else {
        this.message = data.message;
        this.messageClass = 'alert alert-success'
        this.processing = true;
        setTimeout(() => {
          this.router.navigate(['/home']);
        }, 2000);
      }
    });
  }

  checkEmail() {
    let email = this.userForm.get('email').value;
    this.authService.checkEmail(email).subscribe(data => {
      if (!data.success) {
        this.emailValid = false;
        this.emailMessage = data.message;
      } else {
        this.emailValid = true;
        this.emailMessage = data.message;
      }
    });
  }

  checkUsername() {
    let username = this.userForm.get('username').value;
    this.authService.checkUsername(username).subscribe(data => {
      if (!data.success) {
        this.usernameValid = false;
        this.usernameMessage = data.message;
      } else {
        this.usernameValid = true;
        this.usernameMessage = data.message;
      }
    });
  }

  back() {
    this.router.navigate(['/home']);
  }
}
