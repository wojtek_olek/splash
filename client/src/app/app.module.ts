import { BrowserModule }                    from '@angular/platform-browser';
import { NgModule }                         from '@angular/core';
import { HttpModule }                       from '@angular/http';
import { HttpClientModule }                 from '@angular/common/http';
import { JwtModule }                        from '@auth0/angular-jwt';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Router
import { AppRoutingModule }                 from './app-routing.module';
// Components
import { AppComponent }                     from './app.component';
import { HomeComponent }                    from './components/home/home.component';
import { HeaderComponent }                  from './components/header/header.component';
import { FooterComponent }                  from './components/footer/footer.component';
import { DashboardComponent }               from './components/dashboard/dashboard.component';
import { ProfileComponent }                 from './components/profile/profile.component';
import { RegisterComponent }                from './components/register/register.component';
import { LoginComponent }                   from './components/login/login.component';

// User dropdown
import { UserDropdownComponent }            from './components/header/components/user-dropdown/user-dropdown.component';

// ProfileChild
import { PostsComponent }                   from './components/profile/components/posts/posts.component';
import { GalleryComponent }                 from './components/profile/components/gallery/gallery.component';
import { AboutComponent }                   from './components/profile/components/about/about.component';

// Services
import { AuthService }                      from './services/auth.service';

// Guards
import { AuthGuard }                        from './guards/auth.guard';
import { NotAuthGuard }                     from './guards/notAuth.guard';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    ProfileComponent,
    PostsComponent,
    GalleryComponent,
    AboutComponent,
    RegisterComponent,
    HomeComponent,
    LoginComponent,
    UserDropdownComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:4000']
      }
    }),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    NotAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
