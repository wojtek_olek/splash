import { NgModule }                   from '@angular/core';
import { Routes, RouterModule }       from '@angular/router';
// Guards
import { AuthGuard }                  from './guards/auth.guard';
import { NotAuthGuard }               from './guards/notAuth.guard';
// Components
import { HomeComponent }              from './components/home/home.component';
import { DashboardComponent }         from './components/dashboard/dashboard.component';
import { ProfileComponent }           from './components/profile/profile.component';
// ProfileChild
import { PostsComponent }             from './components/profile/components/posts/posts.component';
import { GalleryComponent }           from './components/profile/components/gallery/gallery.component';
import { AboutComponent }             from './components/profile/components/about/about.component';

import { RegisterComponent }          from './components/register/register.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [NotAuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [NotAuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], children: [
    {path: '', component: PostsComponent },
    {path: 'gallery', component: GalleryComponent },
    {path: 'about', component: AboutComponent }
  ]},
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
