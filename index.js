const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const databaseConfig = require('./config/database');
const path = require('path');
const authentication = require('./routes/authentication')(router);
const bodyParser = require('body-parser');
const cors = require('cors');

// Database connection
mongoose.Promise = global.Promise;
mongoose.connect(databaseConfig.uri, (err) => {
  if (err) {
    console.log('Could not connect to database: ' + err);
  } else {
    console.log('Successful connected with: ' + databaseConfig.db)
  }
});

// Static dir for front-end
const corsOptions = {
  origin: 'http://localhost:4200'
};
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/client/dist'));
app.use('/authentication', authentication)

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/dist/index.html'));
});

// Starting server at localhost:4000
app.listen(4000, () => {
  console.log('Splash API running and listening at port 4000 :)')
});