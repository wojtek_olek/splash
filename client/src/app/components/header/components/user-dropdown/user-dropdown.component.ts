import { Component, HostListener, OnInit }    from '@angular/core';
import { AuthService }                from '../../../../services/auth.service';
import { Router }                     from '@angular/router';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html'
})
export class UserDropdownComponent implements OnInit {
  dropdownOpen = false;
  name;

  constructor(private authService: AuthService,
              private router: Router) {}

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.name = profile.user.name;
    })
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  clickedInside($event: Event, number){
    $event.preventDefault();
    $event.stopPropagation();
    this.dropdownOpen = !this.dropdownOpen;
  }
  @HostListener('document:click', ['$event']) clickedOutside($event){
    this.dropdownOpen = false;
  }

}
