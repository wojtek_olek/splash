const User = require("../models/user");
const jsonwt = require("jsonwebtoken");
const databaseConfig = require("../config/database");

module.exports = router => {
  router.post("/register", (req, res) => {
    if (!req.body.email) {
      res.json({ success: false, message: "You must provide an e-mail" });
    } else {
      if (!req.body.username) {
        res.json({ success: false, message: "You must provide a username" });
      } else {
        if (!req.body.password) {
          res.json({ success: false, message: "You must provide a password" });
        } else {
          if (!req.body.name) {
            res.json({ success: false, message: "You must provide a name" });
          } else {
            if (!req.body.vorname) {
              res.json({
                success: false,
                message: "You must provide a vorname"
              });
            } else {
              let user = new User({
                email: req.body.email.toLowerCase(),
                username: req.body.username.toLowerCase(),
                password: req.body.password,
                name: req.body.name,
                vorname: req.body.vorname
              });
              user.save(err => {
                if (err) {
                  if (err.code === 11000) {
                    res.json({
                      success: false,
                      message: "Username or e-mail already exists"
                    });
                  } else {
                    if (err.errors) {
                      if (err.errors.email) {
                        res.json({
                          success: false,
                          message: err.errors.email.message
                        });
                      } else {
                        if (err.errors.username) {
                          res.json({
                            success: false,
                            message: err.errors.username.message
                          });
                        } else {
                          if (err.errors.password) {
                            res.json({
                              success: false,
                              message: err.errors.password.message
                            });
                          } else {
                            res.json({ success: false, message: err });
                          }
                        }
                      }
                    } else {
                      res.json({
                        success: false,
                        message: "Could not save user. Error: ",
                        err
                      });
                    }
                  }
                } else {
                  res.json({ success: true, message: "Acount registered!" });
                }
              });
            }
          }
        }
      }
    }
  });

  router.get("/checkEmail/:email", (req, res) => {
    if (!req.params.email) {
      res.json({ success: false, message: "E-mail was not provided!" });
    } else {
      let reqEmail = req.params.email.toLowerCase();
      User.findOne({ email: reqEmail }, (err, user) => {
        if (err) {
          res.json({ success: false, message: err });
        } else {
          if (user) {
            res.json({ success: false, message: "E-mail has already taken" });
          } else {
            res.json({ success: true, message: "E-mail is available" });
          }
        }
      });
    }
  });

  router.get("/checkUsername/:username", (req, res) => {
    if (!req.params.username) {
      res.json({ success: false, message: "Username was not provided!" });
    } else {
      let reqUsername = req.params.username.toLowerCase();
      User.findOne({ username: reqUsername }, (err, user) => {
        if (err) {
          res.json({ success: false, message: err });
        } else {
          if (user) {
            res.json({ success: false, message: "Username has already taken" });
          } else {
            res.json({ success: true, message: "Username is available" });
          }
        }
      });
    }
  });

  router.post("/login", (req, res) => {
    if (!req.body.username) {
      res.json({ success: false, message: "No username was provided" });
    } else {
      if (!req.body.password) {
        res.json({ success: false, message: "No password was provided" });
      } else {
        User.findOne(
          { username: req.body.username.toLowerCase() },
          (err, user) => {
            if (err) {
              res.json({ success: false, message: err });
            } else {
              if (!user) {
                res.json({ success: false, message: "Username not found" });
              } else {
                let validPassword = user.comparePasswords(req.body.password);
                if (!validPassword) {
                  res.json({ success: false, message: "Incorrect password" });
                } else {
                  let token = jsonwt.sign(
                    { userId: user._id },
                    databaseConfig.secret,
                    { expiresIn: "24h" }
                  );
                  res.json({
                    success: true,
                    message: "Success!",
                    token: token,
                    user: { username: user.username, email: user.email }
                  });
                }
              }
            }
          }
        );
      }
    }
  });

  // Routes that use autohorization via token
  router.use((req, res, next) => {
    let token = req.headers["authorization"];
    if (!token) {
      res.json({ success: false, message: "No token provided!" });
    } else {
      jsonwt.verify(token, databaseConfig.secret, (err, decoded) => {
        if (err) {
          res.json({ success: false, message: "Token invalid " + err });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    }
  });

  router.get("/profile", (req, res) => {
    User.findOne({ _id: req.decoded.userId })
      .select("username email name vorname")
      .exec((err, user) => {
        if (err) {
          res.json({ success: false, message: err });
        } else {
          if (!user) {
            res.json({ success: false, message: "User not found" });
          } else {
            res.json({ success: true, user: user });
          }
        }
      });
  });

  return router;
};
