import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {
  server = "http://localhost:4000";
  authToken;
  user;
  options;

  constructor(private http: Http,
              private jwtHelperService: JwtHelperService) {}

  createAuthHeaders() {
    this.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    });
  }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  registerUser(user) {
    return this.http
      .post(this.server + "/authentication/register", user)
      .map(res => res.json());
  }

  checkUsername(username) {
    return this.http
      .get(this.server + "/authentication/checkUsername/" + username)
      .map(res => res.json());
  }

  checkEmail(email) {
    return this.http
      .get(this.server + "/authentication/checkEmail/" + email)
      .map(res => res.json());
  }

  login(user) {
    return this.http.post(this.server + '/authentication/login', user).map(res => res.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  getProfile() {
    this.createAuthHeaders();
    return this.http.get(this.server + '/authentication/profile', this.options).map(res => res.json());
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  loggedIn() {
    const token: string = this.jwtHelperService.tokenGetter()

    if (!token) {
      return false
    }

    const tokenExpired: boolean = this.jwtHelperService.isTokenExpired(token)

    return !tokenExpired
  }

}
