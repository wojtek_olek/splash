import { Component, OnInit }                                from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators }  from '@angular/forms';
import { AuthService }                                      from '../../services/auth.service';
import { Router }                                           from '@angular/router';
import { AuthGuard }                                        from '../../guards/auth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  message;
  messageClass;
  processing;
  userForm: FormGroup;
  previousURL;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private authGuard: AuthGuard) {
    this.createForm();
  }

  ngOnInit() {
    if (this.authGuard.redirectURL) {
      this.messageClass = 'alert alert-error';
      this.message = 'You must be logged to view this page!';
      this.previousURL = this.authGuard.redirectURL;
      this.authGuard.redirectURL = undefined;
    }
  }

  createForm() {
    this.userForm = this.formBuilder.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required]
      }
    )
  }

  onLoginSubmit() {
    this.processing = true;
    let user = {
      username: this.userForm.get('username').value,
      password: this.userForm.get('password').value
    }
  
    this.authService.login(user).subscribe(data => {
      if (!data.success) {
        this.messageClass = 'alert alert-error';
        this.message = data.message;
        this.processing = false;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        this.authService.storeUserData(data.token, data.user);
        setTimeout(() => {
          if (this.previousURL) {
            this.router.navigate([this.previousURL]);
          } else {
            this.router.navigate(['/dashboard']);
          }
        }, 2);
      }
    })
  }
}
